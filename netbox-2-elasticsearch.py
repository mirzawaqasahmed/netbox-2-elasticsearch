import pynetbox
import os

from datetime import datetime
from pytz import timezone as py_timezone
import pytz

from elasticsearch import Elasticsearch

TIME_ZONE=os.getenv('TIME_ZONE', 'America/Santiago')
NETBOX_URL=os.getenv('NETBOX_URL', 'http://localhost:8000')
NETBOX_PRIVATE_KEY=os.getenv('NETBOX_PRIVATE_KEY', False)
NETBOX_TOKEN=os.getenv('NETBOX_TOKEN', 'd6f4e314a5b5fefd164995169f28ae32d987704f')
ELASTICSEARCH = os.getenv('ELASTICSEARCH', 'http://localhost:9200')

nb = pynetbox.api(
    'http://localhost:8000',
    private_key_file=NETBOX_PRIVATE_KEY,
    token='d6f4e314a5b5fefd164995169f28ae32d987704f'
)

class DateUTC(object):
    '''
    class used to convert local datetime to UTC.
    we can put UTC date inside elasticsearch. Its better!
    '''
    def __init__(self, time_zone):
        self.pytimezone = pytz.timezone(time_zone)
        self.utc = pytz.utc

    def datetime_to_utc(self, dateobj):
        utc_date = dateobj.astimezone(self.utc)
        utc_date_stamp = int(utc_date.timestamp())
        utc_date_els = int(utc_date.timestamp() * 1000)

        return {
            'utc_date': utc_date,
            'utc_date_stamp': utc_date_stamp,
            'utc_date_els': utc_date_els
        }

class Netbox2Elastic(object):
    '''
    sync netbox with elasticsearch
    '''

    def __init__(self):
        self.nb = pynetbox.api(
            NETBOX_URL,
            private_key_file=NETBOX_PRIVATE_KEY,
            token=NETBOX_TOKEN
        )

        self.es = Elasticsearch(ELASTICSEARCH)
        self.els_date = DateUTC(TIME_ZONE)


    def nb_2_els(self, nb_obj, nb_type):
        '''
        receive a nb obj
        create a els obj
        save on elasticsearch
        '''

        now = datetime.now()
        els_vm_date = self.els_date.datetime_to_utc(now)

        els_obj = {
            'nb_type': nb_type,
            'hostname': nb_obj.name,
            'status': nb_obj.status.label,
            'g_last_mod_date': els_vm_date['utc_date_els'],
            'g_country': nb_obj.site.region.name,
            'g_local': nb_obj.site.name,
            'tags': nb_obj.tags,
            'comments': nb_obj.comments
        }

        ## role
        try:
            els_obj['role'] = nb_obj.role.name
        except:
            pass

        ## group name
        try:
            els_obj['g_businessunit'] = nb_obj.tenant.group.name
        except:
            pass

        ## tenant name
        try:
            els_obj['g_flag'] = nb_obj.tenant.name
        except:
            pass

        ## vm attributes
        try:
            els_obj['cluster_group'] = nb_obj.cluster.group.name
            els_obj['cluster_type'] = nb_obj.cluster.type.name
            els_obj['cluster'] = nb_obj.cluster.name
            els_obj['memory'] = nb_obj.memory
            els_obj['vcpus'] = nb_obj.vcpus
        except:
            pass

        ## devices attributes
        try:
            els_obj['serial'] = nb_obj.serial
        except:
            pass
        try:
            els_obj['asset_tag'] = nb_obj.asset_tag
        except:
            pass

        ## platform
        try:
            els_obj['platform'] = nb_obj.platform.name
        except:
            pass
        
        ## custom_fields
        try:
            els_obj['custom_fields'] = nb_obj.custom_fields
        except:
            pass

        ## primary_ip
        try:
            els_obj['primary_ip'] = nb_obj.primary_ip.address
        except:
            pass

        els_id = els_obj['nb_type'] + '-' + nb_obj.name + '-' + now.strftime("%d-%m-%y")

        self.es.index(index='netbox-mirror', id=els_id, body=els_obj)

    def get_vms(self):
        '''
        get all virtual machines and send one by one to elasticsearch
        '''
        for nb_obj in self.nb.virtualization.virtual_machines.all():
            self.nb_2_els(nb_obj, 'vm')

    def get_devices(self):
        '''
        get all devices and send one by one to elasticsearch
        '''
        for nb_obj in self.nb.dcim.devices.all():
            self.nb_2_els(nb_obj, 'device')

nb2els = Netbox2Elastic()

nb2els.get_devices()
nb2els.get_vms()

