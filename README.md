netbox-2-elasticsearch
======================

# Integration between Netbox and Elasticsearch

## env variables

* TIME_ZONE='America/Sao_Paulo'
* NETBOX_URL='https://netbox.sample.corp'
* NETBOX_TOKEN='d6f4e314a5b5fefd164995169f28ae32d987704f'
* NETBOX_PRIVATE_KEY='/path/to/key.pem' # private key is optional

## Usage

```
$ export TIME_ZONE='America/Sao_Paulo'
$ export NETBOX_URL='https://netbox.sample.corp'
$ export NETBOX_PRIVATE_KEY='/path/to/key.pem' # private key is optional
$ export NETBOX_TOKEN='d6f4e314a5b5fefd164995169f28ae32d987704f'
$ pip3 install -r requirements.txt
$ python3 netbox-2-elasticsearch.py
```

